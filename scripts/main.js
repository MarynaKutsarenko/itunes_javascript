import { videoPlayerInit } from "./videoPlayer.js";
import { radioPlayerInit } from "./radioPlayer.js";


const playerBtn = document.querySelectorAll('.player-btn');
const playerBlock = document.querySelectorAll('.player-block');
const titleTemp = document.querySelector('.temp');


const deactivateBtn = () => {
  titleTemp.style.display = 'none';
  playerBtn.forEach(btn => { btn.classList.remove('active') });
  playerBlock.forEach(element => { element.classList.remove('active') });
}

playerBtn.forEach((btn, index) => {
  btn.addEventListener('click', () => {
    deactivateBtn();
    btn.classList.add('active');
    playerBlock[index].classList.add('active');
  })
});


videoPlayerInit();
radioPlayerInit();
